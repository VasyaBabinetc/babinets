﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using LibraryServer.Domain.DatabaseConfiguration;
using LibraryServer.RequestModels;
using Newtonsoft.Json;

namespace LibraryServer
{
    class Program
    {
        public static IPHostEntry ipHost = Dns.GetHostEntry("localhost");
        public static IPAddress ipAddr = ipHost.AddressList[0];
        public static int port = 11000;
        public static IPEndPoint ipEndPoint = new(ipAddr, port);
        public static Socket sender = new(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

        static void Main(string[] args)
        {

            Console.Write("Server started\n");
            
            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);
                while (true)
                {
                    Socket handler = sListener.Accept();
                    string data = "";
                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    Console.Write("Conneced with client\nReceived Data: " + data + "\n");

                    var requestData = data.Split(new[] { '\r', '\n' });
                    Type type = typeof(Methods);
                    MethodInfo method = type.GetMethod(requestData[0]);
                    Methods c = new Methods();
                    string result = (string)method.Invoke(c, new object[] { requestData[1]});

                    byte[] msg = Encoding.UTF8.GetBytes(result);
                    handler.Send(msg);
                    if (data.IndexOf("<TheEnd>") > -1)
                    {
                        Console.WriteLine("Server disconnected from client.");
                        break;
                    }
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    Console.Write("Connection with client is closed\n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
