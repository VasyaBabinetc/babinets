﻿using LibraryServer.Domain.DatabaseConfiguration;
using LibraryServer.RequestModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LibraryServer.Domain.Entities;

namespace LibraryServer
{
    public class Methods
    {
        public string Login(string loginInfo)
        {
            var login = JsonConvert.DeserializeObject<LoginModel>(loginInfo);
            using (LibraryDbContext dbContext = new())
            {
                var user = dbContext.Users.FirstOrDefault(x => x.Username == login.Login && x.Password == login.Password);
                if (user == null)
                {
                    return 0.ToString();
                }
                else
                {
                    return user.Id.ToString();
                }
            }
        }

        public string Register(string registrInfo)
        {
            var registr = JsonConvert.DeserializeObject<RegistrationModel>(registrInfo);
            using (LibraryDbContext dbContext = new())
            {
                var user = dbContext.Users.FirstOrDefault(x => x.Username == registr.Login);
                if (user == null)
                {
                    var newUser = new UserEntity
                    {
                        Name = registr.Name,
                        Username = registr.Login,
                        Password = registr.Password,
                        PhoneNumber = registr.PhoneNumber
                    };
                    dbContext.Users.Add(newUser);
                    dbContext.SaveChanges();
                    return true.ToString();
                }
                else
                {
                    return false.ToString();
                }
            }
        }

        public string SearchBookByAuthor(string searchInfo)
        {
            var search = JsonConvert.DeserializeObject<SearchingByAuthor>(searchInfo);
            using (LibraryDbContext dbContext = new())
            {
                var books = dbContext.Books.Where(x => x.UserId == null && x.Author.Name.Contains(search.SearchOption)).Select(x => new 
                {
                    Id = x.Id,
                    Name = x.Name,
                    Author = x.Author.Name,
                    Year = x.Year.Year
                }).ToList();

                var result = JsonConvert.SerializeObject(books);
                return result;
            }
        }

        public string SearchBookByName(string searchInfo)
        {
            var search = JsonConvert.DeserializeObject<SearchingByBookName>(searchInfo);
            using (LibraryDbContext dbContext = new())
            {
                var books = dbContext.Books.Where(x => x.UserId == null && x.Name.Contains(search.SearchOption)).Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name,
                    Author = x.Author.Name,
                    Year = x.Year.Year
                }).ToList();
                var result = JsonConvert.SerializeObject(books);
                return result;
            }
        }

        public string Booking(string bookingInfo)
        {
            var bookedBook = JsonConvert.DeserializeObject<Booking>(bookingInfo);
            using (LibraryDbContext dbContext = new())
            {
                var book = dbContext.Books.FirstOrDefault(x => x.Id == bookedBook.BookId);
                if (book != null)
                {
                    book.UserId = bookedBook.ClientId;
                    dbContext.SaveChanges();
                    return true.ToString();
                }
                else
                {
                    return false.ToString();
                }
            }
        }
    }
}
