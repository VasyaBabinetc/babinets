﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryServer.RequestModels
{
    public class RegistrationModel
    {
        public string Login { set; get; }
        public string Password { set; get; }
        public string Name { set; get; }
        public string PhoneNumber { set; get; }
    }
}
