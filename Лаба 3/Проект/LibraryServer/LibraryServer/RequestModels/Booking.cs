﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryServer.RequestModels
{
    class Booking
    {
        public int BookId { set; get; }
        public int ClientId { set; get; }
    }
}
