﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryServer.Domain.Entities
{
    public class BookEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AuthorId { get; set; }
        public int? UserId { get; set; }
        public DateTime Year { get; set; }
        public AuthorEntity Author { get; set; }
        public UserEntity Owner { get; set; }
    }
}
