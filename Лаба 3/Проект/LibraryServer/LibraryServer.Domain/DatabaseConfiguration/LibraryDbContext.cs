﻿using LibraryServer.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryServer.Domain.DatabaseConfiguration
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<AuthorEntity> Authors { get; set; }
        public DbSet<BookEntity> Books { get; set; }

        public LibraryDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=LibraryDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<AuthorEntity>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<BookEntity>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<BookEntity>().HasOne(x => x.Owner).WithMany(x => x.BookedBooks).HasForeignKey(x => x.UserId);
            modelBuilder.Entity<BookEntity>().HasOne(x => x.Author).WithMany(x => x.Books).HasForeignKey(x => x.AuthorId);

            SeedData.Seed(modelBuilder);
        }
    }
}
