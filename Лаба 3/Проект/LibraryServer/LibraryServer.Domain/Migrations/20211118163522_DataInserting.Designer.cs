﻿// <auto-generated />
using System;
using LibraryServer.Domain.DatabaseConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace LibraryServer.Domain.Migrations
{
    [DbContext(typeof(LibraryDbContext))]
    [Migration("20211118163522_DataInserting")]
    partial class DataInserting
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.12")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("LibraryServer.Domain.Entities.AuthorEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Authors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Іван Франко"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Леся Українка"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Тарас Шевченко"
                        },
                        new
                        {
                            Id = 4,
                            Name = "Сергій Жадан"
                        },
                        new
                        {
                            Id = 5,
                            Name = "Толкін"
                        },
                        new
                        {
                            Id = 6,
                            Name = "Даніель Дефо"
                        });
                });

            modelBuilder.Entity("LibraryServer.Domain.Entities.BookEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("UserId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Year")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("UserId");

                    b.ToTable("Books");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 1,
                            Name = "Перехресні стежки",
                            UserId = 1,
                            Year = new DateTime(1901, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 2,
                            AuthorId = 1,
                            Name = "Перехресні стежки",
                            Year = new DateTime(2002, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 3,
                            AuthorId = 1,
                            Name = "Зів'яле листя",
                            Year = new DateTime(1989, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 4,
                            AuthorId = 2,
                            Name = "Лісова пісня",
                            Year = new DateTime(1997, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 5,
                            AuthorId = 2,
                            Name = "Лісова пісня",
                            Year = new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 6,
                            AuthorId = 2,
                            Name = "Contra spem spero",
                            Year = new DateTime(2010, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 7,
                            AuthorId = 3,
                            Name = "Катерина",
                            Year = new DateTime(1978, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 8,
                            AuthorId = 3,
                            Name = "Кобзар",
                            Year = new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 9,
                            AuthorId = 3,
                            Name = "Кобзар",
                            Year = new DateTime(2007, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 10,
                            AuthorId = 4,
                            Name = "Ворошиловград",
                            Year = new DateTime(2013, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 11,
                            AuthorId = 4,
                            Name = "Тамплієри",
                            Year = new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 12,
                            AuthorId = 4,
                            Name = "Цитатник",
                            Year = new DateTime(2010, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 13,
                            AuthorId = 5,
                            Name = "Гоббіт",
                            Year = new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 14,
                            AuthorId = 5,
                            Name = "Гоббіт",
                            Year = new DateTime(2007, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 15,
                            AuthorId = 5,
                            Name = "Володар перстнів.Хранителі перстня",
                            Year = new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 16,
                            AuthorId = 5,
                            Name = "Володар перстнів.Дві вежі",
                            Year = new DateTime(2017, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 17,
                            AuthorId = 5,
                            Name = "Володар перстнів.Повернення короля",
                            Year = new DateTime(2015, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 18,
                            AuthorId = 6,
                            Name = "Робінзон Крузо",
                            Year = new DateTime(1999, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 19,
                            AuthorId = 6,
                            Name = "Робінзон Крузо",
                            Year = new DateTime(2006, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 20,
                            AuthorId = 1,
                            Name = "Герой поневолі",
                            Year = new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 21,
                            AuthorId = 2,
                            Name = "З людської намови.Проза",
                            Year = new DateTime(2015, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        });
                });

            modelBuilder.Entity("LibraryServer.Domain.Entities.UserEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Username")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Admin",
                            Password = "admin123",
                            Username = "admin"
                        });
                });

            modelBuilder.Entity("LibraryServer.Domain.Entities.BookEntity", b =>
                {
                    b.HasOne("LibraryServer.Domain.Entities.AuthorEntity", "Author")
                        .WithMany("Books")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("LibraryServer.Domain.Entities.UserEntity", "Owner")
                        .WithMany("BookedBooks")
                        .HasForeignKey("UserId");

                    b.Navigation("Author");

                    b.Navigation("Owner");
                });

            modelBuilder.Entity("LibraryServer.Domain.Entities.AuthorEntity", b =>
                {
                    b.Navigation("Books");
                });

            modelBuilder.Entity("LibraryServer.Domain.Entities.UserEntity", b =>
                {
                    b.Navigation("BookedBooks");
                });
#pragma warning restore 612, 618
        }
    }
}
