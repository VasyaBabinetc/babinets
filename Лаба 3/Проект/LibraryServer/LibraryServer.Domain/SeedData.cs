﻿using LibraryServer.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace LibraryServer.Domain
{
    public static class SeedData
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            var user = new UserEntity { Id = 1, Name = "Admin", Password = "admin123", Username = "admin"};
            var author = new List<AuthorEntity>
            {
                new AuthorEntity() { Id = 1, Name = "Іван Франко" },
                new AuthorEntity() { Id = 2, Name = "Леся Українка" },
                new AuthorEntity() { Id = 3, Name = "Тарас Шевченко" },
                new AuthorEntity() { Id = 4, Name = "Сергій Жадан" },
                new AuthorEntity() { Id = 5, Name = "Толкін" },
                new AuthorEntity() { Id = 6, Name = "Даніель Дефо" },
            };
            var book = new List< BookEntity>
            {
                new BookEntity() { Id = 1, Name = "Перехресні стежки", AuthorId = 1, UserId = 1, Year = new DateTime(1901, 1, 1) },
                new BookEntity() { Id = 2, Name = "Перехресні стежки", AuthorId = 1, Year = new DateTime(2002, 1, 1) },
                new BookEntity() { Id = 3, Name = "Зів'яле листя", AuthorId = 1, Year = new DateTime(1989, 1, 1) },
                new BookEntity() { Id = 4, Name = "Лісова пісня", AuthorId = 2, Year = new DateTime(1997, 1, 1) },
                new BookEntity() { Id = 5, Name = "Лісова пісня", AuthorId = 2, Year = new DateTime(1990, 1, 1) },
                new BookEntity() { Id = 6, Name = "Contra spem spero", AuthorId = 2, Year = new DateTime(2010, 1, 1) },
                new BookEntity() { Id = 7, Name = "Катерина", AuthorId = 3, Year = new DateTime(1978, 1, 1) },
                new BookEntity() { Id = 8, Name = "Кобзар", AuthorId = 3, Year = new DateTime(1990, 1, 1) },
                new BookEntity() { Id = 9, Name = "Кобзар", AuthorId = 3, Year = new DateTime(2007, 1, 1) },
                new BookEntity() { Id = 10, Name = "Ворошиловград", AuthorId = 4, Year = new DateTime(2013, 1, 1) },
                new BookEntity() { Id = 11, Name = "Тамплієри", AuthorId = 4, Year = new DateTime(2016, 1, 1) },
                new BookEntity() { Id = 12, Name = "Цитатник", AuthorId = 4, Year = new DateTime(2010, 1, 1) },
                new BookEntity() { Id = 13, Name = "Гоббіт", AuthorId = 5, Year = new DateTime(1950, 1, 1) },
                new BookEntity() { Id = 14, Name = "Гоббіт", AuthorId = 5, Year = new DateTime(2007, 1, 1) },
                new BookEntity() { Id = 15, Name = "Володар перстнів.Хранителі перстня", AuthorId = 5, Year = new DateTime(2016, 1, 1) },
                new BookEntity() { Id = 16, Name = "Володар перстнів.Дві вежі", AuthorId = 5, Year = new DateTime(2017, 1, 1) },
                new BookEntity() { Id = 17, Name = "Володар перстнів.Повернення короля", AuthorId = 5, Year = new DateTime(2015, 1, 1) },
                new BookEntity() { Id = 18, Name = "Робінзон Крузо", AuthorId = 6, Year = new DateTime(1999, 1, 1) },
                new BookEntity() { Id = 19, Name = "Робінзон Крузо", AuthorId = 6, Year = new DateTime(2006, 1, 1) },
                new BookEntity() { Id = 20, Name = "Герой поневолі", AuthorId = 1, Year = new DateTime(2016, 1, 1) },
                new BookEntity() { Id = 21, Name = "З людської намови.Проза", AuthorId = 2, Year = new DateTime(2015, 1, 1) }
            };
            modelBuilder.Entity<UserEntity>().HasData(user);
            modelBuilder.Entity<AuthorEntity>().HasData(author);
            modelBuilder.Entity<BookEntity>().HasData(book);
        }
    }
}
