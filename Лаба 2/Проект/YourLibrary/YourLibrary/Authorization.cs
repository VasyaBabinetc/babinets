﻿using Newtonsoft.Json;
using System;
using System.Windows;

namespace YourLibrary
{
    public class Authorization
    {
        public int Id { set; get; }
        public string Login { set; get; }
        public string Password { set; get; }
        public string Name { set; get; }
        public string PhoneNumber { set; get; }
        public bool Authorized  = false;
        public bool IsInDataBase  = false;
        public bool Success = false;
        public void CheckClient(Authorization client)//звертання до сервера
        {
            var serializedModel = JsonConvert.SerializeObject(client);
            var request = string.Join("\n", "Login", serializedModel);
            var response = ServerCalller.Send(request);
            if ((response == null)||(int.Parse(response[0]) == 0))
            {
                client.IsInDataBase = false;
            }
            else 
            {
                client.Id = int.Parse(response[0]);
                client.IsInDataBase = true;
            }
            if (client.IsInDataBase)
            {
                client.Authorized = true;
            }
        }

        public void RegisterClient(Authorization client)
        {
            var serializedModel = JsonConvert.SerializeObject(client);
            var request = string.Join("\n", "Register", serializedModel);
            var response = ServerCalller.Send(request);
            if (response != null)
            {
                client.Success = Convert.ToBoolean(response[0]);
            }
            else
            {
                MessageBox.Show("Sorry, server is unavailable");
            }
        }
    }
}
