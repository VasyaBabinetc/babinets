﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YourLibrary
{
    public class Book
    {
        public int Id { set; get; }
        public string Author { set; get; }
        public string Name { set; get; }
        public int Year { set; get; }
    }
}
