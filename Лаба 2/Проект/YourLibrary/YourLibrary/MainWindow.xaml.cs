﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace YourLibrary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        AuthorizationWindow authorizationWindow = new AuthorizationWindow();
        Authorization client = new Authorization();
        Search searching = new Search();
        Booking booking = new Booking();
        Book selBook = new Book();

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            StartButton.Visibility = Visibility.Hidden;
            StartText.Visibility = Visibility.Hidden;
            ChoosingText.Visibility = Visibility.Visible;
            LogInButton.Visibility = Visibility.Visible;
            SearchButton.Visibility = Visibility.Visible;
        }

        private void LogInButton_Click_(object sender, RoutedEventArgs e)
        {
            authorizationWindow.Owner = this;
            authorizationWindow.Closing += new CancelEventHandler(authorizationWindow_Closed);
            authorizationWindow.Show();
        }

        void authorizationWindow_Closed(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            authorizationWindow.Visibility = Visibility.Hidden;
            authorizationWindow.LoginText.Text = string.Empty;
            authorizationWindow.PasswordText.Clear();
            client = authorizationWindow.client;
            if (client == null)
            {
                LogInButton.Visibility = Visibility.Visible;
            }
            else if (client.Authorized==true)
            {
                LogInButton.Visibility = Visibility.Hidden;
                LogOutButton.Visibility = Visibility.Visible;
            }
            if (searching.FoundBooks.Any())
            {
                FoundBooksList.SelectedIndex = -1;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {

            SearchButton.Visibility = Visibility.Hidden;
            ChoosingText.Visibility = Visibility.Hidden;
            StartImage.Visibility = Visibility.Hidden;
            SearchStartText.Visibility = Visibility.Visible;
            SearchTextInput.Visibility = Visibility.Visible;
            SearchByAuthor_RadButton.Visibility = Visibility.Visible;
            SearchByBookName_RadButton.Visibility = Visibility.Visible;
            FoundBooksList.Visibility = Visibility.Visible;
            StartSearchButton.Visibility = Visibility.Visible;
        }

        private void StartSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(SearchTextInput.Text)==true)
                {
                    MessageBox.Show("Введіть текст для пошуку");
                }
                else if ((SearchByBookName_RadButton.IsChecked == false) && (SearchByAuthor_RadButton.IsChecked == false))
                {
                    MessageBox.Show("Оберіть за яким параметром відбувається пошук");
                }
                else
                {
                    //var res = ServerCalller.Send("hello");
                    searching.FoundBooks.Clear();
                    if (SearchByBookName_RadButton.IsChecked == true)
                    {
                        searching.SearchingByBookName = true;
                    }
                    if (SearchByAuthor_RadButton.IsChecked == true)
                    {
                        searching.SearchingByBookName = false;
                    }
                    searching.SearchOption = SearchTextInput.Text;
                    searching.SearchingBooks(searching);
                    FoundBooksList.ItemsSource = searching.FoundBooks;
                    if (searching.FoundBooks.Count == 0)
                    {
                        MessageBox.Show("Перепрошуємо, нічого не було знайдено");
                    }
                }
            }
            catch(Exception w)
            {
                MessageBox.Show("Виникла помилка під час пошуку");
                MessageBox.Show(w.ToString());
            }
        }

        private void FoundBooksList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int index = FoundBooksList.SelectedIndex;
            if (index != -1)
            {
                selBook = new Book();
                string Info = "Графік роботи бібліотеки:\nПонеділок-П'ятниця з 9:00 по 19:00\nСубота з 10:00 по 16:00\nНеділя вихідний";

                selBook = FoundBooksList.Items.GetItemAt(index) as Book;
                if (client.Authorized)
                {
                    booking.BookId = selBook.Id;
                    booking.ClientId = client.Id;
                    bool result = booking.BookThis(booking);

                    if (result)
                    {
                        MessageBox.Show($"Вітаємо,ви забронювали книгу {selBook.Author} {selBook.Name}\n{Info}");
                    }
                    else
                    {
                        MessageBox.Show("Перепрошуємо, виникла помилка, спробуйте, будь лакска, пізніше ще раз");
                    }
                }
                else
                {
                    MessageBox.Show("Авторизуйтесь, будь ласка, для бронювання книги");
                }
            }
        }

        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {
            LogInButton.Visibility = Visibility.Visible;
            LogOutButton.Visibility = Visibility.Hidden;
            client.Authorized = false;
            client.Id = 0;
        }
    }
}
