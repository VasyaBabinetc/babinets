﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace YourLibrary
{
    public class Booking
    {
        public int BookId { set; get; }
        public int ClientId { set; get; }

        public bool BookThis(Booking book)//звертання до сервера
        {
            var serializedModel = JsonConvert.SerializeObject(book);
            var request = string.Join("\n", "Booking", serializedModel);
            var response = ServerCalller.Send(request);
            if (response != null)
            {
                return Convert.ToBoolean(response[0]);
            }
            else
            {
                return false;
            }
        }
    }
}
