﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace YourLibrary
{
    public static class ServerCalller
    {
        public static string[] Send(string s)
        {
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            int port = 11000;
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
            Socket sender1 = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sender1.Connect(ipEndPoint);
                byte[] bytes = new byte[1024];
                string message = s;
                bytes = Encoding.UTF8.GetBytes(message);
                sender1.Send(bytes);
                message = "";
                while (true)
                {
                    int bytesRec = sender1.Receive(bytes);
                    if (bytesRec == 0)
                        break;
                    else
                    {
                        message += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    }

                }
                //MessageBox.Show(message);
                sender1.Shutdown(SocketShutdown.Both);
                sender1.Close();
                return message.Split(';');
            }
            catch (Exception ex)
            {
                MessageBox.Show("Sorry, server is unavailable");
                return null;
            }
        }
    }
}
