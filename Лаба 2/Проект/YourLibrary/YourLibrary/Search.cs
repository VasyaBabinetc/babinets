﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Windows;

namespace YourLibrary
{
    public class Search
    {
        public string SearchOption { set; get; }
        public bool SearchingByBookName { set; get; }//якщо false пошук по імені автора

        public ObservableCollection<Book> FoundBooks { set; get; } = new ObservableCollection<Book>();

        public void SearchingBooks(Search search)
        {
            var serializedModel = JsonConvert.SerializeObject(search);
            string[] response;
            if (search.SearchingByBookName)
            {
                var request = string.Join("\n", "SearchBookByName", serializedModel);
                response = ServerCalller.Send(request);
            }
            else 
            {
                var request = string.Join("\n", "SearchBookByAuthor", serializedModel);
                response = ServerCalller.Send(request);
            }
            if (response != null)
            {
                var books = JsonConvert.DeserializeObject<List<Book>>(response[0]);
                foreach (Book b in books)
                {
                    FoundBooks.Add(b);
                }
            }
        }
    }

}
