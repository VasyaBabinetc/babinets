﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace YourLibrary
{
    /// <summary>
    /// Interaction logic for AuthorizationWindow.xaml
    /// </summary>
    public partial class AuthorizationWindow : Window
    { 
        public AuthorizationWindow()
        {
            InitializeComponent();
            LoginText.Text = string.Empty;
            PasswordText.Clear();

        }
        public Authorization client;
        private void LogInButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(LoginText.Text) || string.IsNullOrWhiteSpace(PasswordText.Password.ToString()))
            {
                MessageBox.Show("Не введено логін або пароль");
            }
            else
            {
                client = new Authorization { Login = LoginText.Text, Password = PasswordText.Password.ToString() };
                client.CheckClient(client);
                if (client.Authorized)
                {
                    MessageBox.Show("Вхід виконано");
                    Close();
                }
                else
                {
                    MessageBox.Show("Неправильно введено логін або пароль");
                }
            }
        }
        private void RegistrationButton_Click(object sender, RoutedEventArgs e)
        {
            StartText.Visibility = Visibility.Hidden;
            LoginText.Visibility = Visibility.Hidden;
            LogInButton.Visibility = Visibility.Hidden;
            PasswordText.Visibility = Visibility.Hidden;
            RegistrationButton.Visibility = Visibility.Hidden;
            RegistrationText.Visibility = Visibility.Hidden;
            StartRegistrationText.Visibility = Visibility.Visible;
            NewLoginBox.Visibility = Visibility.Visible;
            NewLoginText.Visibility = Visibility.Visible;
            NewNameBox.Visibility = Visibility.Visible;
            NewNameText.Visibility = Visibility.Visible;
            NewNumberBox.Visibility = Visibility.Visible;
            NewNumberText.Visibility = Visibility.Visible;
            NewPasswordBox.Visibility = Visibility.Visible;
            NewPasswordText.Visibility = Visibility.Visible;
            RegistrationConfirmButton.Visibility = Visibility.Visible;
        }

        private void RegistrationConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            Authorization client;
            if (string.IsNullOrWhiteSpace(NewLoginBox.Text) || string.IsNullOrWhiteSpace(NewPasswordBox.Password.ToString())||string.IsNullOrWhiteSpace(NewNameBox.Text) || string.IsNullOrWhiteSpace(NewNumberBox.Text))
            {
                MessageBox.Show("Заповніть усі поля, будь ласка");
            }
            else
            {
                client = new Authorization { Login = NewLoginBox.Text, Password = NewPasswordBox.Password.ToString(), Name = NewNameBox.Text, PhoneNumber = NewNumberBox.Text };
                client.RegisterClient(client);
                if (client.Success)
                {
                    MessageBox.Show("Вас зареєстровано, увійдіть у свій профіль, будь ласка");
                    LoginText.Text = string.Empty;
                    PasswordText.Clear();
                    StartText.Visibility = Visibility.Visible;
                    LoginText.Visibility = Visibility.Visible;
                    LogInButton.Visibility = Visibility.Visible;
                    PasswordText.Visibility = Visibility.Visible;
                    RegistrationButton.Visibility = Visibility.Visible;
                    RegistrationText.Visibility = Visibility.Visible;
                    StartRegistrationText.Visibility = Visibility.Hidden;
                    NewLoginBox.Visibility = Visibility.Hidden;
                    NewLoginText.Visibility = Visibility.Hidden;
                    NewNameBox.Visibility = Visibility.Hidden;
                    NewNameText.Visibility = Visibility.Hidden;
                    NewNumberBox.Visibility = Visibility.Hidden;
                    NewNumberText.Visibility = Visibility.Hidden;
                    NewPasswordBox.Visibility = Visibility.Hidden;
                    NewPasswordText.Visibility = Visibility.Hidden;
                    RegistrationConfirmButton.Visibility = Visibility.Hidden;
                }
                else
                {
                    MessageBox.Show("Логін вже зайнятий, введіть, будь ласка, інший");
                }
            }
        }

        private Boolean IsTextAllowed(String text)
        {
            foreach (Char c in text.ToCharArray())
            {
                if (Char.IsDigit(c) || Char.IsControl(c)) continue;
                else return false;
            }
            return true;
        }
        private void NewNumberBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void NewNumberBox_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
    }
}
